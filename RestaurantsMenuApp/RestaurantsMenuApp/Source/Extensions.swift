//
//  Extensions.swift
//  RestaurantsMenuApp
//
//  Created by Pedro Danilo Ferreira Veloso on 29/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import UIKit

extension UIViewController {
    static func initialize<T: UIViewController>(as: T.Type) -> T {
        return T(nibName: "\(T.self)", bundle: nil)
    }
}

extension Double {
    var toCurrency: String? {
        let value = NSDecimalNumber(value: self)
        let nf = NumberFormatter()
        nf.locale = Locale.autoupdatingCurrent
        nf.numberStyle = .currency
        return nf.string(from: value) ?? nil
    }
}
