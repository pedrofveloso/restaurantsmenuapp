//
//  ProductListPresenter.swift
//  RestaurantsMenuApp
//
//  Created by Pedro Danilo Ferreira Veloso on 29/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import Foundation

protocol ProductListPresenterDelegate: AnyObject {
    func viewDidLoad()
    func callWaiter()
    var numberOfProducts: Int { get }
    func product(atIndex index: Int) -> Product
}

class ProductListPresenter {
    var business: ProductListBusinessDelegate
    var products: [Product] = []
    weak var view: ProductListViewDelegate?
    
    init(business: ProductListBusinessDelegate) {
        self.business = business
    }
    
    private func fetch() {
        business.fetch { [weak self] products in
            guard let self = self else { return }
            self.products = products
            self.view?.reloadData()
        }
    }
}

extension ProductListPresenter: ProductListPresenterDelegate {
    var numberOfProducts: Int {
        return products.count
    }
    
    func viewDidLoad() {
        fetch()
        view?.setTitle(text: business.titleText)
        view?.setCallWaiterHidden(isHidden: !business.shouldShowCallWaiter)
    }
    
    func callWaiter() {
        if business.shouldShowCallWaiter {
            business.callWaiter()
            view?.showFeedback(title: "Garçom!!!!", message: "Seu garçom foi informado e está se dirigindo a sua mesa!")
        }
    }
    
    func product(atIndex index: Int) -> Product {
        return products[index]
    }
}
