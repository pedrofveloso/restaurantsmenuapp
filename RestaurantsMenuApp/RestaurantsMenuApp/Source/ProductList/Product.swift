//
//  Product.swift
//  RestaurantsMenuApp
//
//  Created by Pedro Danilo Ferreira Veloso on 29/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import Foundation

struct Product: Codable {
    let name: String
    let price: Double
    let type: String
}
