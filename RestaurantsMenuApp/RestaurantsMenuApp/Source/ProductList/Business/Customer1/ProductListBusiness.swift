//
//  ProductListBusiness.swift
//  Customer1
//
//  Created by Pedro Danilo Ferreira Veloso on 11/06/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import Foundation

class ProductListBusiness: ProductListBaseBusiness {
    
    override func fetch(completion: @escaping ProductListSuccess) {
        // do something before
        super.fetch(completion: completion)
    }
    
    override var titleText: String {
        return "Customer 1 - Products"
    }
    
    
}
