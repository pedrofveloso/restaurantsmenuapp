//
//  ProductListBusiness.swift
//  Customer2
//
//  Created by Pedro Danilo Ferreira Veloso on 12/06/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import Foundation

class ProductListBusiness: ProductListBaseBusiness {
    
    override var shouldShowCallWaiter: Bool {
        return false
    }
    
}
