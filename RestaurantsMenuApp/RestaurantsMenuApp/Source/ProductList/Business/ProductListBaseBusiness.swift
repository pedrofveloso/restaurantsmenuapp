//
//  ProductListBaseBusiness.swift
//  RestaurantsMenuApp
//
//  Created by Pedro Danilo Ferreira Veloso on 29/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import Foundation

typealias ProductListSuccess = ([Product]) -> Void

protocol ProductListBusinessDelegate {
    func fetch(completion: @escaping ProductListSuccess)
    func callWaiter()
    
    var shouldShowCallWaiter: Bool { get }
    var titleText: String { get }
}

class ProductListBaseBusiness: ProductListBusinessDelegate {
    
    func fetch(completion: @escaping ProductListSuccess) {
        // call some api from a shared repository class
        guard let path = Bundle.main.path(forResource: "ProductList", ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else { return }
        let products = try? JSONDecoder().decode([Product].self, from: data)
        completion(products ?? [])
    }
    
    var shouldShowCallWaiter: Bool {
        return true
    }
    
    var titleText: String {
        return "Restaurants Menu App" 
    }
    
    func callWaiter() {
        //do something
    }
}
