//
//  ProductListViewController.swift
//  RestaurantsMenuApp
//
//  Created by Pedro Danilo Ferreira Veloso on 29/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import UIKit

class ProductListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
        }
    }
    @IBOutlet weak var callWaiterButton: UIButton! {
        didSet {
            callWaiterButton.setTitle("Chamar garçom", for: .normal)
        }
    }
    
    @IBAction func callWaiter() {
        presenter?.callWaiter()
    }
    
    var presenter: ProductListPresenterDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
}

extension ProductListViewController: ProductListViewDelegate {
    func reloadData() {
        tableView.reloadData()
    }
    
    func setCallWaiterHidden(isHidden: Bool) {
        callWaiterButton.isHidden = isHidden
    }
    
    func setTitle(text: String) {
        navigationItem.title = text
    }
    
    func showFeedback(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Voltar", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

extension ProductListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.numberOfProducts ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: nil)
        let product = presenter?.product(atIndex: indexPath.row)
        cell.textLabel?.text = product?.name
        cell.detailTextLabel?.text = product?.price.toCurrency ?? " - "
        return cell
    }

}
