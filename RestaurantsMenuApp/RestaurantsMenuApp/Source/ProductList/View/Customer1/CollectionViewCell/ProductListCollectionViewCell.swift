//
//  ProductListCollectionViewCell.swift
//  Customer1
//
//  Created by Pedro Danilo Ferreira Veloso on 11/06/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import UIKit

class ProductListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
            nameLabel.textAlignment = .center
            nameLabel.lineBreakMode = .byWordWrapping
            nameLabel.numberOfLines = 0
        }
    }
    @IBOutlet weak var priceLabel: UILabel! {
        didSet {
            priceLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
            priceLabel.textAlignment = .center
            priceLabel.textColor = .gray
        }
    }
    
    func setup(product: Product) {
        nameLabel.text = product.name
        priceLabel.text = product.price.toCurrency
    }

}
