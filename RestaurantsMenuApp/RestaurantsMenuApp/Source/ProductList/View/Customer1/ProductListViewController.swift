//
//  ProductListViewController.swift
//  Customer1
//
//  Created by Pedro Danilo Ferreira Veloso on 11/06/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import UIKit

class ProductListViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.register(UINib(nibName: "\(ProductListCollectionViewCell.self)", bundle: nil), forCellWithReuseIdentifier: "\(ProductListCollectionViewCell.self)")
        }
    }
    
    var presenter: ProductListPresenterDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }

    private func addWaiterButton() {
        let waiterButton = UIBarButtonItem(title: "Garçom!", style: .plain, target: self, action: #selector(callWaiter))
        self.navigationItem.setRightBarButton(waiterButton, animated: true)
    }
    
    @objc private func callWaiter() {
        // do something
    }
}

extension ProductListViewController: ProductListViewDelegate {
    func reloadData() {
        collectionView.reloadData()
    }
    
    func setCallWaiterHidden(isHidden: Bool) {
        //For this customer button will always be shown
        addWaiterButton()
    }
    
    func setTitle(text: String) {
        navigationItem.title = text
    }
    
    func showFeedback(title: String, message: String) {
        // call a toast alert on top of the screen
    }
}

extension ProductListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.numberOfProducts ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(ProductListCollectionViewCell.self)", for: indexPath) as? ProductListCollectionViewCell, let product = presenter?.product(atIndex: indexPath.row) else {
            return UICollectionViewCell()
        }
        
        cell.setup(product: product)
        return cell
    }
}

extension ProductListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
}

extension ProductListViewController: UINavigationControllerDelegate {
    
}
