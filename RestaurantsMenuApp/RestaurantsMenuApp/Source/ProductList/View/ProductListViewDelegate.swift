//
//  ProductListViewDelegate.swift
//  RestaurantsMenuApp
//
//  Created by Pedro Danilo Ferreira Veloso on 29/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import Foundation

protocol ProductListViewDelegate: AnyObject {
    func reloadData()
    func setCallWaiterHidden(isHidden: Bool)
    func setTitle(text: String)
    func showFeedback(title: String, message: String)
}
