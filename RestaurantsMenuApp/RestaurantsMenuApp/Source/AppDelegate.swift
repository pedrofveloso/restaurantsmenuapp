//
//  AppDelegate.swift
//  RestaurantsMenuApp
//
//  Created by Pedro Danilo Ferreira Veloso on 29/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let vc = UIViewController.initialize(as: ProductListViewController.self)
        let presenter = ProductListPresenter(business: ProductListBusiness())
        vc.presenter = presenter
        presenter.view = vc
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: vc)
        window?.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {}

    func applicationDidEnterBackground(_ application: UIApplication) {}

    func applicationWillEnterForeground(_ application: UIApplication) {}

    func applicationDidBecomeActive(_ application: UIApplication) {}

    func applicationWillTerminate(_ application: UIApplication) {}


}

